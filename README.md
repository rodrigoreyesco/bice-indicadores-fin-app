# Indicadores Económicos - Desafío Bice Labs

## Descripción
Aplicación frontend para indicaores economicos

## Instalación

```bash
$ npm install
```

## Ejecutar app en Development server

Ejecutar `ng serve` para inicializar el servidor de desarrollo. 
Luego navegar a `http://localhost:4200/`.

## Build para producción

Ejecutar `ng build --prod` para construir el proyecto para producción. los componentes resultantes seran almacenados en la carpeta `dist/`.

## Build en Google Cloud Run

```bash
gcloud builds submit --tag gcr.io/civil-listener-275722/indicadores-bice-app
```

## Ejecución de Unit tests

Ejecutar `ng test` para ejecutar los test unitarios via [Karma](https://karma-runner.github.io).

## Autor

  rodrigo.reyesco@gmail.com

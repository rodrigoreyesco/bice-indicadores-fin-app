import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndicadoresComponent } from './indicadores/indicadores.component';


const routes: Routes = [
  { path: 'indicadores', component: IndicadoresComponent },
  { path: '', redirectTo: '/indicadores', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private subject = new Subject<any>();

  constructor() { }

  sendEvent(row: any) {
    this.subject.next(row);
  }

  getEvent(): Observable<any> {
    return this.subject.asObservable();
  }

}

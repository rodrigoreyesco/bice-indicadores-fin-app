import { AfterContentInit, Component, OnInit, ViewChild } from '@angular/core';
import { IndicadoresService } from './indicadores.service';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { SharedService } from '../shared/shared.service';
import { Subscription } from 'rxjs';
import { ChartService } from '../shared/chart.service';
import { MatGridList } from '@angular/material/grid-list';
import { MediaChange, MediaObserver } from '@angular/flex-layout';

@Component({
  selector: 'app-indicadores',
  templateUrl: './indicadores.component.html',
  styleUrls: ['./indicadores.component.scss']
})
export class IndicadoresComponent implements OnInit {

  clickRowEventSubscription: Subscription;
  columnHeader = { 'key': 'Indicador', 'value': 'Valor', 'unit': 'Unidad', 'date': 'Fecha', 'name': 'Descripción' };
  tableData: any;
  now: any = new Date().toISOString();
  chart: any;
  breakpoint: any;

  @ViewChild('grid') grid: MatGridList;

  gridByBreakpoint = {
    xl: 2,
    lg: 2,
    md: 2,
    sm: 2,
    xs: 1
  }

  gridRowHeight = {
    xl: "90%",
    lg: "90%",
    md: "90%",
    sm: "4:7",
    xs: "4:7"
  }

  constructor(private indicadoresService: IndicadoresService,
    private sharedService: SharedService,
    private chartService: ChartService,
    private mediaObserver: MediaObserver) { }

  ngOnInit(): void {

    this.clickRowEventSubscription = this.sharedService.getEvent()
      .subscribe((row: any) => {
        this.loadDetailChart(row);
      });

    this.indicadoresService.getUltimosIndicadores()
      .pipe(
        map((data: any) => {
          data.map((row: any) => {
            row.key = row.key.toUpperCase();
            row.date = moment.unix(row.date).format("DD/MM/YYYY");
            row.value = this.formatValue(row);
            return row;
          })
          return data;
        })
      ).subscribe((finalData: any) => {
        this.tableData = finalData;
      });
  }

  ngAfterContentInit() {
    this.mediaObserver.media$.subscribe((change: MediaChange) => {
      this.grid.rowHeight = this.gridRowHeight[change.mqAlias];
      this.grid.cols = this.gridByBreakpoint[change.mqAlias];
    });
  }

  loadDetailChart(row: any) {
    this.indicadoresService
      .getHistorialIndicadoresPorElemento(row.key.toLowerCase())
      .subscribe((data: any) => {
        this.chartService.sendEvent(data);
      })
  }

  formatValue(row: any) {
    if (row.unit === 'dolar') {
      return `$ ${row.value} USD`;
    }
    else if (row.unit === 'pesos') {
      return `$ ${row.value} CLP`;
    }
    else if (row.unit === 'porcentual') {
      return `${row.value} %`;
    }
    else {
      return `${row.value}`;
    }
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IndicadoresService {

  private indicadoresEconomicosUrl: string = environment.indicadoresEconomicosUrl;

  constructor(private http: HttpClient) { }

  /**
   * obtiene un listado de los ultimos indicadores
   */
  getUltimosIndicadores(){
    return this.http.get(`${this.indicadoresEconomicosUrl}/ultimos`);
  }

  /**
   * obtiene un historial de indicadores por elemento
   */
  getHistorialIndicadoresPorElemento(elemento: string){
    return this.http.get(`${this.indicadoresEconomicosUrl}/historial/${elemento}`);
  }
}

import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { GoogleChartComponent, ChartType } from 'angular-google-charts';
import { Subscription } from 'rxjs';
import { ChartService } from '../shared/chart.service';
import * as moment from 'moment';

@Component({
  selector: 'app-common-chart',
  templateUrl: './common-chart.component.html',
  styleUrls: ['./common-chart.component.scss']
})
export class CommonChartComponent implements OnInit {

  chartEventSubscription: Subscription;
  chartData: any = {};
  chartElement: any;

  myOptions = {
    colors: ['#e0440e', '#e6693e', '#ec8f6e', '#f3b49f', '#f6c7b6'],
    is3D: true
  };

  constructor(private chartService: ChartService) { }

  @ViewChild('chart')
  public chart: GoogleChartComponent;

  ngOnInit(): void {
    this.chartEventSubscription = this.chartService.getEvent()
      .subscribe((data: any) => {
        this.init(data);
      });
  }

  init(data: any) {
    this.chartData.name = data.name;
    this.chartData.unit = data.unit;
    this.chartData.data = this.prepareData(data.values);
    this.updateChart(this.chartData);
  }

  updateChart(chartData: any): void {
    this.chartElement = {
      title: chartData.name,
      type: ChartType.AreaChart,
      columns: ['Fecha', `Valor (${chartData.unit})`],
      data: chartData.data
    }
  }

  prepareData(values: any) {
    let result = Object.keys(values).map((key: any) => {
      return [(moment.unix(key).format("DD/MM/YYYY")), values[key]];
    });
    return result;
  }

}

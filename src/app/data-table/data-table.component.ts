import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { SharedService } from '../shared/shared.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() tableData: any;
  @Input() columnHeader: any;
  objectKeys = Object.keys;
  dataSource: any;
  selection = new SelectionModel<any>(false, null);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private sharedService: SharedService) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.tableData);
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectRow(row: any) {
    this.sharedService.sendEvent(row);
  }

}

FROM node:latest as node
# set working directory
WORKDIR /app
COPY . .
RUN npm install -g @angular/cli
RUN npm install
RUN npm run build --prod

FROM nginx:alpine as nginx
COPY nginx/default.conf /etc/nginx/conf.d/
COPY --from=node /app/dist/bice-indicadores-fin-app usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
